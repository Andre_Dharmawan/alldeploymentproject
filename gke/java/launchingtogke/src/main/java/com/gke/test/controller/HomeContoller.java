package com.gke.test.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
public class HomeContoller {

	@GetMapping("home")
	public String HomeScreen() {
		return "ini home screen GKE";
	}
}
