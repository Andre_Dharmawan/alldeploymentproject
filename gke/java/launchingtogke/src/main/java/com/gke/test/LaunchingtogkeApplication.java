package com.gke.test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LaunchingtogkeApplication {

	public static void main(String[] args) {
		SpringApplication.run(LaunchingtogkeApplication.class, args);
	}

}
